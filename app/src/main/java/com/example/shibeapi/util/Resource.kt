package com.example.shibeapi.util

import com.example.shibeapi.model.Shibe

sealed class Resource(data: List<Shibe>?, errorMsg: String?) {
    data class Success(val data: List<Shibe>) : Resource(data, null)
    object Loading : Resource(null, null)
    data class Error(val errorMsg: String) : Resource(null, errorMsg)
}