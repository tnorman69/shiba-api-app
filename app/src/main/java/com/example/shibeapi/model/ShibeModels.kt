package com.example.shibeapi.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "my_shibes")
data class Shibe (
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val url: String = ""
)