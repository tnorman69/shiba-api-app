package com.example.shibeapi.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Shibe::class], exportSchema = false, version = 69)
abstract class shibeDB: RoomDatabase() {

    abstract fun shibeDao(): ShibeDao

    companion object {
        private val DB_NAME = "ShibeDB"

        fun getInstance(context: Context) =
            Room.databaseBuilder(context, shibeDB::class.java, DB_NAME)
                .fallbackToDestructiveMigration().build()
    }
}