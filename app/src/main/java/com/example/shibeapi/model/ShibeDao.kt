package com.example.shibeapi.model

import android.view.ViewDebug
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ShibeDao {

    @Query("SELECT * FROM my_shibes")
    fun getShibes(): List<Shibe>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addShibe(shibes: List<Shibe>)
}