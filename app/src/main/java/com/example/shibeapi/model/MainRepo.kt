package com.example.shibeapi.model

import android.content.Context
import com.example.shibeapi.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainRepo(val context: Context) {

    private val apiService by lazy { ApiService.retrofitInstance }
    private val shibeDao = shibeDB.getInstance(context).shibeDao()

    suspend fun getShibes() = withContext(Dispatchers.IO) {
        return@withContext try {

            val response = apiService.getShibes()
            val dbInfo = shibeDao.getShibes()

            if (response.isSuccessful && response.body()!!.size > dbInfo.size) {
                val shibes = response.body()!!.map { Shibe(url = it) }
                shibeDao.addShibe(shibes)
                Resource.Success(shibeDao.getShibes())
            } else {
                Resource.Success(dbInfo)
            }
        } catch(e: Exception) {
            Resource.Error(e.localizedMessage.toString())
        }
    }
}