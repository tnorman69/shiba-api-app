package com.example.shibeapi.view

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.shibeapi.databinding.ListItemBinding
import com.example.shibeapi.model.Shibe

class LovelyAdapter: RecyclerView.Adapter<LovelyAdapter.LovelyViewholder>() {

    private lateinit var data: List<Shibe>

    class LovelyViewholder(
        private val binding: ListItemBinding
    ): RecyclerView.ViewHolder(binding.root) {
        fun applyToItem(item: String) {
            binding.shibeImage.loadImage(item)
            binding.shibeUrlText.text = item
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LovelyViewholder {
        val binding = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LovelyViewholder(binding)
    }

    override fun onBindViewHolder(holder: LovelyViewholder, position: Int) {
        val item = data[position]
        holder.applyToItem(item.url)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun applyData(data: List<Shibe>) {
        this.data = data
    }

}

fun ImageView.loadImage(url: String) {
    Glide.with(context).load(url).into(this)
}
