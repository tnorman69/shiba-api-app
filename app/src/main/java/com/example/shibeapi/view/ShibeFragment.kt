package com.example.shibeapi.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.shibeapi.databinding.FragmentShibeBinding
import com.example.shibeapi.model.MainRepo
import com.example.shibeapi.util.Resource
import com.example.shibeapi.viewmodel.MainViewmodel
import com.example.shibeapi.viewmodel.MainViewmodelFactory

class ShibeFragment : Fragment() {

    private var _binding: FragmentShibeBinding? = null
    private val binding: FragmentShibeBinding get() = _binding!!

    private val repo by lazy {
        MainRepo(requireContext())
    }

    private val viewmodel by viewModels<MainViewmodel>{
        MainViewmodelFactory(repo)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeBinding.inflate(inflater, container, false). also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding) {
        recycle.layoutManager = GridLayoutManager(context, 1)
        viewmodel.viewState.observe(viewLifecycleOwner) { viewState ->
            when (viewState) {
                is Resource.Error -> {}
                Resource.Loading -> {}
                is Resource.Success -> {
                    recycle.adapter = LovelyAdapter().apply {
                        applyData(viewState.data)
                    }
                }
            }
        }
    }
}