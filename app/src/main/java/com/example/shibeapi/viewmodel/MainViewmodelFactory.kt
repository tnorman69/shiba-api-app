package com.example.shibeapi.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.shibeapi.model.MainRepo

class MainViewmodelFactory(
    private val repo: MainRepo
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewmodel(repo) as T
    }
}