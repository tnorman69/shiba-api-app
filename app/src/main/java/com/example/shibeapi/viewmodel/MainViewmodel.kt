package com.example.shibeapi.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shibeapi.model.MainRepo
import com.example.shibeapi.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewmodel(val repo: MainRepo): ViewModel() {

    private val _viewState: MutableLiveData<Resource> = MutableLiveData(Resource.Loading)
    val viewState: LiveData<Resource> get() = _viewState

    init {
        viewModelScope.launch(Dispatchers.Main) {
            _viewState.value = repo.getShibes()
        }
    }

}